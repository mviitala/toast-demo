import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { ToastOptions } from '@ionic/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  
  ngOnInit(): void {

    this.presentToast("ngOnInit was executed", 2000, "middle");     
  }

  constructor(public toastController : ToastController)  {}

  private showToast(){

    this.presentToast("Toast button was clicked", 2000, "top"); 
  }

  async presentToast(m : string, s : number, p : string) {
    const toast = await this.toastController.create({
      message: m,
      duration: s,
      /*if this line won't compile, use first a hardcoded "top", modify to use p argument and re-compile*/
      position: p
    });
    toast.present();
  }
  

}
